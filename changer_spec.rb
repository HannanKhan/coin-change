require 'rspec'
require_relative 'changer'

describe 'Changer' do
  #initializing changer
  changer = Changer.new
  #generating a random number based on timeseed
  srand Time.new.to_f
  random_num = rand 100

  it 'should generate result for dynamic always >= greedy' do
    expect(changer.dynamic(random_num)).to be >= changer.greedy(random_num)
  end
end