class Changer

  def initialize(coins = nil)
    #initialize coins
    @coins = coins || [25, 10, 5, 1]
    @coins.sort! { |x, y| y <=> x}
  end

  def dynamic(coin = 0, change)
    #base case termination
    if change == 0
      return 0
    end

    #coin is not valid => inf
    if coin > change
      return 1000
    end

    #calculate recursive solutions for sub-change using coins and take minimum
    @coins.map { |coin| 1 + dynamic(coin, change - coin) }.min

  end

  def greedy(change)

    coins = 0

    @coins.map do |coin|
      coins += (change/ coin).floor
      change %= coin
    end

    coins
  end

end